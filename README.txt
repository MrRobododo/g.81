Yo o/
---------------------------------------------------
Ce doc est un rappel pour pouvoir utiliser GITFORK

---------------------------------------------------

Pour actualiser Fork il faut clicker sur le bouton FETCH ( situer en haut à gauche ) 

---------------------------------------------------

Si vous avez des changements a appliquer sur le drive
Vous devez COMMIT un changement en local
    1.Pour cela il faut aller dans CHANGES
    2.Selectionner les fichiers a modifier (ou à ajouter) dans la section UNSTAGED en doublant clicant dessus
    3.Entrer un nom au COMMIT dans la fenetre en bas à droite (ENTER COMMIT SUBJECT) 

--------------------------------------------------

Une fois que le COMMIT en local est réaliser il vous reste plus qu'à PUSH sur la branch ORIGINE MASTER

/ ! \ PENSEZ TOUJOURS A PULL LA BRANCH AVANT DE PUSH

Si vous avez des questions, n'hesiter pas a m'envoyer un message ou à lire le document Initiation à git présent sur le salon 
Discord : TEAM/GIT/Initiation_a_GIT.pdf
